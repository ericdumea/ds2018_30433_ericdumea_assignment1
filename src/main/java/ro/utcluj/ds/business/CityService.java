package ro.utcluj.ds.business;

import com.fasterxml.jackson.databind.ObjectMapper;
import ro.utcluj.ds.repository.CityRepository;
import ro.utcluj.ds.repository.domain.City;
import ro.utcluj.ds.repository.domain.dto.CityInformation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CityService {

    private final String API_URL = "http://api.geonames.org/timezoneJSON?formatted=true&";
    private final String API_POSTFIX = "&username=ericd&style=full";
    private final String LATITUDE_PREFIX = "lat=";
    private final String LONGITUDE_PREFIX = "&lng=";
    CityRepository cityRepository = new CityRepository();

    public City findByName(String name) {
        return cityRepository.findByName(name);
    }

    public LocalDateTime getLocalTime(City city) {

        ObjectMapper objectMapper = new ObjectMapper();

        String apiUrl = API_URL + LATITUDE_PREFIX + city.getLatitude() + LONGITUDE_PREFIX + city.getLongitude() + API_POSTFIX;

        StringBuilder stringBuilder = new StringBuilder();

        DateTimeFormatter dTF= DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm");

        HttpURLConnection conn=null;
        BufferedReader reader=null;
        try{
            //Declare the connection to weather api url
            URL url = new URL(apiUrl);
            conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("HTTP GET Request Failed with Error code : "
                        + conn.getResponseCode());
            }

            //Read the content from the defined connection
            //Using IO Stream with Buffer raise highly the efficiency of IO
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String output = null;
            while ((output = reader.readLine()) != null)
                stringBuilder.append(output);
        }catch(MalformedURLException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        } finally {
            if(reader!=null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(conn!=null)
            {
                conn.disconnect();
            }
        }

        CityInformation cityInformation = new CityInformation();

        try {
            cityInformation = objectMapper.readValue(stringBuilder.toString(),CityInformation.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return LocalDateTime.parse(cityInformation.getTime(),dTF);
    }

}
