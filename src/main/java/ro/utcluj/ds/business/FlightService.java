package ro.utcluj.ds.business;

import ro.utcluj.ds.repository.FlightRepository;
import ro.utcluj.ds.repository.domain.City;
import ro.utcluj.ds.repository.domain.Flight;
import ro.utcluj.ds.repository.domain.dto.FlightDto;

import java.util.List;

public class FlightService {

    FlightRepository flightRepository = new FlightRepository();

    CityService cityService = new CityService();

    public List<Flight> getAllFlights() {
        return flightRepository.findAll();
    }

    public Long insert(FlightDto flightDto) {
        City arrival = cityService.findByName(flightDto.getArrival());
        City departure = cityService.findByName(flightDto.getDeparture());

        Flight flight = new Flight();
        flight.setFlightNumber(flightDto.getFlightNumber());
        flight.setArrival(arrival);
        flight.setDeparture(departure);
        flight.setTimeOfArrival(flightDto.getTimeOfArrival());
        flight.setTimeOfDeparture(flightDto.getTimeOfDeparture());
        flight.setAirplaneType(flightDto.getAirplaneType());

        return flightRepository.insert(flight);
    }

    public void update(FlightDto flightDto) {
        City arrival = cityService.findByName(flightDto.getArrival());
        City departure = cityService.findByName(flightDto.getDeparture());

        Flight flight = new Flight();
        flight.setId(flightDto.getId());
        flight.setFlightNumber(flightDto.getFlightNumber());
        flight.setArrival(arrival);
        flight.setDeparture(departure);
        flight.setTimeOfArrival(flightDto.getTimeOfArrival());
        flight.setTimeOfDeparture(flightDto.getTimeOfDeparture());
        flight.setAirplaneType(flightDto.getAirplaneType());

        flightRepository.update(flight);
    }

    public Flight findById(Long id){
        return flightRepository.find(id);
    }

    public void delete(Long did) {
        Flight flight = findById(did);
        flightRepository.delete(flight);
    }
}
