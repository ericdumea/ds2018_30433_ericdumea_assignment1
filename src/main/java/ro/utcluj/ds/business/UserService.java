package ro.utcluj.ds.business;

import ro.utcluj.ds.repository.UserRepository;
import ro.utcluj.ds.repository.domain.User;

import java.util.List;

public class UserService {

    UserRepository userRepository = new UserRepository();

    public List<User> getAllUsers() {
        userRepository.findAll();
        return null;
    }


    public User getUserByEmailAndPassword(String username, String password) {
        User user = userRepository.findByEmail(username);
        if(! user.getPassword().equals(password)){
            return null;
        }
        return user;
    }
}
