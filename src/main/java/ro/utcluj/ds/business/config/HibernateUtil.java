package ro.utcluj.ds.business.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ro.utcluj.ds.repository.domain.City;
import ro.utcluj.ds.repository.domain.Flight;
import ro.utcluj.ds.repository.domain.User;

public class HibernateUtil {
    private static final String CONFIG_FILE_NAME = "/hibernate.cfg.xml";
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            configuration.configure(CONFIG_FILE_NAME);
            configuration.addAnnotatedClass(User.class);
            configuration.addAnnotatedClass(Flight.class);
            configuration.addAnnotatedClass(City.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            return configuration.buildSessionFactory(serviceRegistry);
        }
        catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            sessionFactory = buildSessionFactory();
        return sessionFactory;
    }
}
