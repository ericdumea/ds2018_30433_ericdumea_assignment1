package ro.utcluj.ds.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ro.utcluj.ds.business.config.HibernateUtil;
import ro.utcluj.ds.business.error.UserNotFoundException;
import ro.utcluj.ds.repository.domain.Flight;

import java.util.List;

public class FlightRepository {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public Long insert(Flight flight) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        Long id = (Long) currentSession.save(flight);
        transaction.commit();
        return id;
    }

    public Flight find(Long id) {
        Session sf = sessionFactory.getCurrentSession();
        Transaction t = sf.beginTransaction();
        Flight p = sf.get(Flight.class, id);
        t.commit();
        return p;
    }

    public void delete(Flight objectToDelete) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(objectToDelete);
        transaction.commit();
    }

    public void update(Flight objectToUpdate) {
        Flight oldFlight = find(objectToUpdate.getId());
        if(oldFlight == null){
            throw new UserNotFoundException();
        }
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(objectToUpdate);
        transaction.commit();
    }

    public List<Flight> findAll() {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        Query q = currentSession.createQuery("from Flight");
        List<Flight> list = q.list();
        transaction.commit();
        return list;
    }
}
