package ro.utcluj.ds.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ro.utcluj.ds.business.error.UserNotFoundException;
import ro.utcluj.ds.repository.domain.User;
import ro.utcluj.ds.business.config.HibernateUtil;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

public class UserRepository {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public int insert(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.persist(user);
        transaction.commit();
        return 0;
    }

    public User find(int id) {
        Session sf = sessionFactory.getCurrentSession();
        Transaction t = sf.beginTransaction();
        User p = (User) sf.get(User.class, id);
        t.commit();
        return p;
    }

    public void delete(User objectToDelete) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(objectToDelete);
        transaction.commit();
    }

    public void update(User objectToUpdate) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(objectToUpdate);
        transaction.commit();
    }

    public User findByEmail(String email) {
        Session sf = sessionFactory.getCurrentSession();
        Transaction t = sf.beginTransaction();
        Query q = sf.createQuery("from User where email = :email").setString("email", email);
        Optional<User> user = Optional.ofNullable((User) ((org.hibernate.query.Query) q).uniqueResult());
        t.commit();
        return user.orElseThrow(UserNotFoundException::new);
    }

    public List<User> findAll() {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        org.hibernate.query.Query q = currentSession.createQuery("from User");
        List<User> list = q.list();
        transaction.commit();
        return list;
    }
}
