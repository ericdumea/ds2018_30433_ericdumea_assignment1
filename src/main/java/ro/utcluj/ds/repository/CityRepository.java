package ro.utcluj.ds.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ro.utcluj.ds.business.config.HibernateUtil;
import ro.utcluj.ds.business.error.CityNotFoundException;
import ro.utcluj.ds.business.error.UserNotFoundException;
import ro.utcluj.ds.repository.domain.City;
import ro.utcluj.ds.repository.domain.User;

import java.util.List;
import java.util.Optional;

public class CityRepository {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public Long insert(City city) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        Long id = (Long) currentSession.save(city);
        transaction.commit();
        return id;
    }

    public City find(int id) {
        Session sf = sessionFactory.getCurrentSession();
        Transaction t = sf.beginTransaction();
        City city = sf.get(City.class, id);
        t.commit();
        return city;
    }

    public City findByName(String name) {
        Session sf = sessionFactory.getCurrentSession();
        Transaction t = sf.beginTransaction();

        javax.persistence.Query q = sf.createQuery("from City where name = :name").setString("name", name);

        Optional<City> city = Optional.ofNullable((City) ((org.hibernate.query.Query) q).uniqueResult());

        t.commit();
        return city.orElseThrow(CityNotFoundException::new);
    }

    public void delete(City objectToDelete) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(objectToDelete);
        transaction.commit();
    }

    public void update(City objectToUpdate) {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        currentSession.update(objectToUpdate);
        transaction.commit();
    }

    public List<City> findAll() {
        Session currentSession = sessionFactory.getCurrentSession();
        Transaction transaction = currentSession.beginTransaction();
        Query q = currentSession.createQuery("from City");
        List<City> list = q.list();
        transaction.commit();
        return list;
    }

}
