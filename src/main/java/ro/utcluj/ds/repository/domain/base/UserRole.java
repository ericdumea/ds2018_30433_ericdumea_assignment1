package ro.utcluj.ds.repository.domain.base;

public enum UserRole {
    NOT_ASSIGNED,
    ADMIN,
    USER
}
