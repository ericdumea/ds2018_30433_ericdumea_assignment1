package ro.utcluj.ds.repository.domain.base;

public enum AirplaneType {
    SMALL,
    MEDIUM,
    BIG,
    JUMBOJET
}
