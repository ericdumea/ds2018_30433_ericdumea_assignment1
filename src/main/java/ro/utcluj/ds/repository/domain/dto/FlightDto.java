package ro.utcluj.ds.repository.domain.dto;

import ro.utcluj.ds.repository.domain.base.AirplaneType;

import java.time.LocalDateTime;

public class FlightDto {

    private Long id;

    private String departure;

    private String arrival;

    private LocalDateTime timeOfDeparture;

    private LocalDateTime timeOfArrival;

    private String flightNumber;

    private AirplaneType airplaneType;

    public FlightDto() {
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public LocalDateTime getTimeOfDeparture() {
        return timeOfDeparture;
    }

    public void setTimeOfDeparture(LocalDateTime timeOfDeparture) {
        this.timeOfDeparture = timeOfDeparture;
    }

    public LocalDateTime getTimeOfArrival() {
        return timeOfArrival;
    }

    public void setTimeOfArrival(LocalDateTime timeOfArrival) {
        this.timeOfArrival = timeOfArrival;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setTimeOfArrival(String arrivalTime) {
        this.timeOfArrival = LocalDateTime.parse(arrivalTime);
    }

    public void setTimeOfDeparture(String departureTime) {
        this.timeOfDeparture = LocalDateTime.parse(departureTime);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }
}
