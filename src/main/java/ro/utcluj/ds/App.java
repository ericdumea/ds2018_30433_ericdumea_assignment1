package ro.utcluj.ds;

import ro.utcluj.ds.business.CityService;
import ro.utcluj.ds.repository.domain.City;
import ro.utcluj.ds.repository.domain.User;
import ro.utcluj.ds.repository.UserRepository;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {

//        UserRepository userRepository = new UserRepository();
//        User user = new User();
//        user.setEmail("a@a.a");
//        user.setPassword("a");
//        userRepository.insert(user);

        City city = new City();
        city.setName("CLuj");
        city.setLatitude("46.7712");
        city.setLongitude("23.623");
        CityService cityService = new CityService();

        System.out.println(cityService.getLocalTime(city));
    }
}
