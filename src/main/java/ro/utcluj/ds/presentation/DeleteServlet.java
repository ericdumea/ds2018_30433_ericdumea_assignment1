package ro.utcluj.ds.presentation;

import ro.utcluj.ds.business.FlightService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteServlet extends HttpServlet {

    FlightService flightService = new FlightService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        flightService.delete(Long.valueOf(req.getParameter("did")));
    }
}
