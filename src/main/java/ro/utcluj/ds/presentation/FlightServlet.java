package ro.utcluj.ds.presentation;

import ro.utcluj.ds.business.CityService;
import ro.utcluj.ds.business.FlightService;
import ro.utcluj.ds.repository.domain.Flight;
import ro.utcluj.ds.repository.domain.base.AirplaneType;
import ro.utcluj.ds.repository.domain.dto.FlightDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FlightServlet extends HttpServlet {

    FlightService flightService = new FlightService();

    CityService cityService = new CityService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        List<Flight> flights = flightService.getAllFlights();

        out.println("<!DOCTYPE html> <html lang=\"en\">");

        out.println("<head>");
        out.println("<title> Flights </title>");
        out.println("<style>");
        out.println("td {padding:10px;}");
        out.println("body {max-width:1200px; margin: auto;}");
        out.println("</style>");
        out.println("</head>");

        out.println("<body>");
        out.println("<table>");

        out.println("<tr>");
        out.println("<th> Flight Nr </th>");
        out.println("<th> Departure City </th>");
        out.println("<th> Departure Time </th>");
        out.println("<th> Arrival City </th>");
        out.println("<th> Arrival Time </th>");
        out.println("<th> Airplane Type </th>");
        out.println("</tr>");
        for (Flight flight : flights) {
            out.println("<tr>");
            out.println("<td>" + flight.getFlightNumber() + "</td>");
            out.println("<td>" + flight.getDeparture().getName() + "\n" + cityService.getLocalTime(flight.getDeparture()) + "</td>");
            out.println("<td>" + flight.getTimeOfDeparture() + "</td>");
            out.println("<td>" + flight.getArrival().getName() + "\n" + cityService.getLocalTime(flight.getArrival()) + "</td>");
            out.println("<td>" + flight.getTimeOfArrival() + "</td>");
            out.println("<td>" + flight.getAirplaneType().name() + "</td>");
            out.println("</tr>");
        }

        out.println("</table>");
        out.println("</body>");
        out.println("</html>");


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FlightDto flight = new FlightDto();
        flight.setArrival(req.getParameter("arrival"));
        flight.setDeparture(req.getParameter("departure"));
        flight.setTimeOfArrival(req.getParameter("arrivalTime"));
        flight.setTimeOfDeparture(req.getParameter("departureTime"));
        flight.setFlightNumber(req.getParameter("flight-number"));
        flight.setAirplaneType(AirplaneType.valueOf(req.getParameter("airType")));
        flightService.insert(flight);
    }

}
