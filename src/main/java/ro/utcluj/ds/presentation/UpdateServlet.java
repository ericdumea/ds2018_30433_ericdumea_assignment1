package ro.utcluj.ds.presentation;

import ro.utcluj.ds.business.FlightService;
import ro.utcluj.ds.repository.domain.base.AirplaneType;
import ro.utcluj.ds.repository.domain.dto.FlightDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateServlet extends HttpServlet {

    FlightService flightService = new FlightService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FlightDto flight = new FlightDto();
        flight.setId(Long.valueOf(req.getParameter("uid")));
        flight.setArrival(req.getParameter("uarrival"));
        flight.setDeparture(req.getParameter("udeparture"));
        flight.setTimeOfArrival(req.getParameter("uarrivalTime"));
        flight.setTimeOfDeparture(req.getParameter("udepartureTime"));
        flight.setFlightNumber(req.getParameter("uflight-number"));
        flight.setAirplaneType(AirplaneType.valueOf(req.getParameter("uairType")));
        flightService.update(flight);
    }
}
