package ro.utcluj.ds.presentation;

import ro.utcluj.ds.business.UserService;
import ro.utcluj.ds.repository.domain.User;
import ro.utcluj.ds.repository.domain.base.UserRole;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws IOException {
        // add code for GET method
        PrintWriter out = response.getWriter();
        out.println("hello");
    }

    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("username");
        String password = request.getParameter("password");

        if ((!email.equals("") && !password.equals(""))) {
            UserService userService = new UserService();
            User user = userService.getUserByEmailAndPassword(email,password);
            if (user != null) {
                if (!(user.getUserRole() == UserRole.ADMIN)) {
                    request.getSession(true).setAttribute("ROLE",UserRole.USER);
                    createUserPage(response);
                } else {
                    request.getSession(true).setAttribute("ROLE",UserRole.ADMIN);
                    createAdminPage(response);
                }
            } else {
                response.sendRedirect("index.html");
            }
        }

    }

    private void createAdminPage(HttpServletResponse response) throws IOException {
        response.sendRedirect("/admin.html");
    }

    private void createUserPage(HttpServletResponse response) throws IOException {
       response.sendRedirect("/user.html");
    }
}
